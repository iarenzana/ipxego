build_number=`date -u +%Y%m%d.%H%M%S`
git_hash=`git rev-parse HEAD`

all: build

build:
	go build -ldflags "-X 'github.com/iarenzana/ipxego/cmd.build=$(build_number)' -X 'github.com/iarenzana/ipxego/cmd.gitHash=$(git_hash)'"

install:
	go install -ldflags "-X 'github.com/iarenzana/ipxego/cmd.build=$(build_number)' -X 'github.com/iarenzana/ipxego/cmd.gitHash=$(git_hash)'"

clean:
	rm ipxego
