ipxego
==

iPXE server written in Go to serve CoreOS.

Install
--

Go 1.6.0 will be needed to compile from source. Also, [Glide](https://glide.sh/) is used for dependency management, but it shouldn't be necessary to use t hanks to Go's native vendoring.
```bash
git clone gitlab.com:iarenzana/ipxego.git
cd ipxego
make
```

Run
--

```bash
ipxego server --ssh-key [path_to_pub_key]
```

On the machine to boot, run the following commands:

```bash
dhcp
chain http://[IP_PXE_SERVER]:19000/boot
```

Usage
--

```bash
ipxego runs a pxe server that will serve boot scripts to get CoreOS installed.

Usage:
ipxego [command]

Available Commands:
server      Start serving
version     Displays version information

Flags:
-c, --channel string      Core OS default channel (default "stable")
--config string       config file (default is $HOME/.ipxego.yaml)
-h, --help                help for ipxego
-P, --port int            Port on which the server listens (default 19000)
-d, --server-dir string   iPXE ISO directory
-s, --ssh-key string      SSH key to use
-t, --toggle              Help message for toggle

Use "ipxego [command] --help" for more information about a command.
```

License
--
Check out the [LICENSE](https://gitlab.com/iarenzana/ipxego/raw/master/LICENSE)
