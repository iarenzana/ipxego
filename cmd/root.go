package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile, sshKey, serverDir, channel string
var port int

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "ipxego",
	Short: "iPXE server for your CoreOS needs.",
	Long:  `ipxego runs a pxe server that will serve boot scripts to get CoreOS installed.`,
}

//Execute run the whole thing
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	RootCmd.PersistentFlags().IntVarP(&port, "port", "P", 19000, "Port on which the server listens")
	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.ipxego.yaml)")
	RootCmd.PersistentFlags().StringVarP(&sshKey, "ssh-key", "s", "", "SSH key to use")
	RootCmd.PersistentFlags().StringVarP(&serverDir, "server-dir", "d", "", "iPXE ISO directory")
	RootCmd.PersistentFlags().StringVarP(&channel, "channel", "c", "stable", "Core OS default channel")

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	HOME := os.Getenv("HOME")
	if cfgFile != "" { // enable ability to specify config file via flag
		viper.SetConfigFile(cfgFile)
	}

	viper.SetConfigName(".ipxego") // name of config file (without extension)
	viper.AddConfigPath(".")       // First, look in the current directory
	viper.AddConfigPath(HOME)      // adding home directory as second search path
	viper.AddConfigPath("/etc")    // Finally, look in /etc as second search path
	viper.SetConfigType("yaml")    // or viper.SetConfigType("yaml")
	viper.AutomaticEnv()           // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
