package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"text/template"

	"github.com/spf13/cobra"
)

var (
	config map[string]string
	t      *template.Template
)

const ipxeBootScript = `#!ipxe
set base-url {{.BaseUrl}}
kernel ${base-url}/coreos_production_pxe.vmlinuz sshkey="{{.SSHKey}}"
initrd ${base-url}/coreos_production_pxe_image.cpio.gz
boot
`

var baseURL string

// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "Start serving",
	Long:  `Serve iPXE.`,
	Run: func(cmd *cobra.Command, args []string) {

		if channel != "stable" && channel != "alpha" && channel != "beta" {
			log.Println(fmt.Errorf("Wrong CoreOS Channel [stable|alpha|beta]"))
			return
		}
		if channel == "stable" {
			baseURL = "http://stable.release.core-os.net/amd64-usr/current"
		} else if channel == "alpha" {
			baseURL = "http://alpha.release.core-os.net/amd64-usr/current"
		} else if channel == "beta" {
			baseURL = "http://beta.release.core-os.net/amd64-usr/current"
		}
		fmt.Println(sshKey)
		sshKey, err := ioutil.ReadFile(sshKey)
		if err != nil {
			log.Fatal("Error reading key! - ", err)
		}

		config = map[string]string{
			"BaseUrl": baseURL,
			"SSHKey":  string(bytes.TrimSpace(sshKey)),
		}
		t = template.Must(template.New("ipxebootscript").Parse(ipxeBootScript))

		http.HandleFunc("/boot", ipxeBootScriptServer)
		http.Handle("/", http.FileServer(http.Dir(serverDir)))
		hostAndPort := fmt.Sprintf("0.0.0.0:%v", port)
		log.Printf("Starting CoreOS iPXE Server on %s...", hostAndPort)
		log.Fatal(http.ListenAndServe(hostAndPort, nil))
	},
}

func init() {

	RootCmd.AddCommand(serverCmd)

}

func ipxeBootScriptServer(w http.ResponseWriter, r *http.Request) {
	log.Printf("Creating boot script for %s", r.RemoteAddr)
	if err := t.Execute(w, config); err != nil {
		http.Error(w, "Error generating the iPXE boot script", 500)
		return
	}
}
