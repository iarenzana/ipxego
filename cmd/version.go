package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var (
	build   = ""
	gitHash = ""
)

// versionCmd represents the version command
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Displays version information",
	Long:  `Displays version informiation`,
	Run: func(cmd *cobra.Command, args []string) {

		fmt.Printf("Build: %s\n", build)
		fmt.Printf("Git Hash: %s\n", gitHash)
	},
}

func init() {
	RootCmd.AddCommand(versionCmd)

}
